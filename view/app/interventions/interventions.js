'use strict';

angular.module('myApp.interventions', ['ngRoute', 'ui.bootstrap', 'myApp.modal'])

	.config(['$routeProvider', function ($routeProvider, $modal) {
		$routeProvider.when('/interventions', {
			templateUrl: 'interventions/interventions.html',
			controller: 'InterventionsCtrl'
		});
	}])

	.controller('InterventionsCtrl', ['$uibModal', '$http', function ($uibModal, $http) {
		var $ctrl = this;

		$ctrl.label = "";
		$ctrl.description = "";
		$ctrl.name = "";
		$ctrl.place = "";
		$ctrl.date = "";

		function onInit() {
			$http.get('http://localhost:5000/interventions/list').
				then(function (response) {
					$ctrl.interventions = response.data.interventions;
				});
		}

		$ctrl.onDelete = function (id) {
			console.log(id)
			$http.delete('http://localhost:5000/interventions/delete?id=' + id).
				then(function (response) {
					$ctrl.interventions = response.data.interventions;
				});
		}

		$ctrl.definedStatus = function (intervention) {
			let status = 'Brouillon';
			if (intervention.label != undefined && intervention.label != ""
				&& intervention.description != undefined && intervention.description != ""
				&& intervention.name != undefined && intervention.name != ""
				&& intervention.place != undefined && intervention.place != ""
				&& intervention.date != undefined && intervention.date != "") {
				status = 'Validé'
			}

			if (new Date() > new Date(intervention.date)) {
				status = 'Terminé'
			}

			return status
		}

		$ctrl.sortByDate = function (reverse) {
			if (reverse) {
				$ctrl.interventions.sort(function (a, b) {
					return new Date(b.date) - new Date(a.date)
				});
			} else {
				$ctrl.interventions.sort(function (a, b) {
					return new Date(a.date) - new Date(b.date)
				});
			}
		}

		onInit();

		let data = {
			'name': $ctrl.name,
			'label': $ctrl.label,
			'description': $ctrl.description,
			'place': $ctrl.place,
			'date': $ctrl.date
		}

		$ctrl.openForUpdate = function (intervention, size, parentSelector) {

			var parentElem = parentSelector ? angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
			$uibModal.open({
				animation: $ctrl.animationsEnabled,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: '/interventions/modal.html',
				controller: function ($uibModalInstance, $scope) {
					$scope.update = true;
					$scope.name = intervention.name;
					$scope.label = intervention.label;
					$scope.description = intervention.description;
					$scope.place = intervention.place;
					$scope.date = new Date(intervention.date);

					console.log($scope.update)

					$scope.cancel = function () {
						$uibModalInstance.close();
					};

					$scope.onUpdate = function() {
						let data = {
							'name': $scope.name,
							'label': $scope.label,
							'description': $scope.description,
							'place': $scope.place,
							'date': $scope.date
						}
						$http.put('http://localhost:5000/interventions/update?id=' + intervention.id, data).
							then(function (response) {
								$ctrl.interventions = response.data.interventions;
							});
					}
				},
				size: size,
				appendTo: parentElem,
			});
		};

		$ctrl.openForCreate = function (size, parentSelector) {
			var parentElem = parentSelector ? angular.element($document[0].querySelector('.modal-demo ' + parentSelector)) : undefined;
			$uibModal.open({
				animation: $ctrl.animationsEnabled,
				ariaLabelledBy: 'modal-title',
				ariaDescribedBy: 'modal-body',
				templateUrl: '/interventions/modal.html',
				controller: function ($uibModalInstance, $scope) {
					$scope.update = false;
					$scope.cancel = function () {
						$uibModalInstance.close();
					};

					$scope.onCreate = function () {
						let data = {
							'name': $scope.name,
							'label': $scope.label,
							'description': $scope.description,
							'place': $scope.place,
							'date': $scope.date
						};

						$http.post('http://localhost:5000/interventions/create', data).
							then(function (response) {
								$ctrl.interventions = response.data.interventions;
								$uibModalInstance.close();
							});
					}

				},
				size: size,
				appendTo: parentElem,
			});
		};
	}
	]);

