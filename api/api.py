import flask
import json
import io
import uuid

from flask_cors import CORS
from flask import request

app = flask.Flask(__name__)
app.config['DEBUG'] = True

# TODO : Bad prefer reverse proxy with Nginx in prod and configure reverse with webpack,gulp 
CORS(app)


@app.route('/interventions/list', methods=['GET'])
def get_all():
    
    json_data = open ('data.json').read ()
    return json.loads(json_data)

@app.route('/interventions/create', methods=['POST'])
def add():

    json_data = open ('data.json').read ()
    data = json.loads(json_data)
    request.json['id'] = uuid.uuid1().node
    data['interventions'].append(request.json)
    with io.open('data.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(data, ensure_ascii=False))
        
    return data


@app.route('/interventions/update', methods=['PUT'])
def update():
    json_data = open ('data.json').read ()
    data = json.loads(json_data)
    for index, intervention in enumerate(data['interventions']):
        if intervention['id'] == int(request.args['id']):
            data['interventions'][index] = request.json
            data['interventions'][index]['id'] = uuid.uuid1().node
            break
        else:
            index = -1
    with io.open('data.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(data, ensure_ascii=False))
    return data

@app.route('/interventions/delete', methods=['DELETE'])
def delete():
    
    json_data = open ('data.json').read ()
    data = json.loads(json_data)
    for index, intervention in enumerate(data['interventions']):
        if intervention['id'] == int(request.args['id']):
            data['interventions'].pop(index)
            break
        else:
            index = -1
    with io.open('data.json', 'w', encoding='utf-8') as f:
        f.write(json.dumps(data, ensure_ascii=False))
    return data


app.run()
