# Test Nautilux

## Version

***Python 2.7.18***  
***Pip 20.2.3***

***Angular 1.7.9***  
***npm 6.14.8***

***Bootstrap 3.3.7***

## Installation

`cd api`  
`pip install -r requirements.txt`

`cd view`  
`npm install`  

## Getting Started

`run npm start` for a dev server. Navigate to http://localhost:8000/#!/interventions

## Information

> J'ai eu un peu de mal avec la modal. Si j'avais un peu plus de temps je re-factoriserais la gestion de celle-ci question de ne pas avoir du code dupliqué.   
> Je commenterais mon code ce qui n'est pas fait, je m'en excuse mais celui-ci me parait assez lisible.
> Pour finir, j'irais sur https://github.com/johnpapa/angular-styleguide/blob/master/a1/README.md pour appliquer les bests pratices. 